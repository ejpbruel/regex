#!/usr/bin/python3

import os
import re
import sys

def group_to_str(group):
    if group is None:
        return "None, None"
    else:
        return "Some(%d), Some(%d)" % (group[0], group[1])

def test_to_str(test):
    name, pattern, text, groups = test
    group_strs = ", ".join([group_to_str(group) for group in groups])
    return "macros::test_regex!(%s, r\"%s\", \"%s\", %s);" % (name, pattern, text, group_strs)

def read_test_data(file):
    basename, _ = os.path.splitext(os.path.basename(file))
    tests = []
    for index, line in enumerate(open(sys.argv[1], "rb")):
        line = line.decode("utf-8")

        if line[0] == '#':
            continue

        fields = [field.strip() for field in line.split('\t')]
        fields = [field for field in fields if len(field) != 0]
        if not 4 <= len(fields) <= 5:
            continue
        options, pattern, text, group_strs = fields[:4]
        if 'E' not in options or 'i' in options:
            continue

        name = "%s_%d" % (basename, index + 1)
        if pattern == "SAME":
            pattern = tests[-1][1]
        text = "" if text == "NULL" else text
        text = re.sub(r"\\(?![nx])", r"\\\\", text)
        if '$' in options and "\\x" in text:
            continue
        groups = []
        if group_strs == "NOMATCH":
            groups = [None]
        elif ',' in group_strs:
            group_strs = [group_str.strip("()") for group_str in group_strs.split(")(")]
            for group_str in group_strs:
                start, end = [bound_str.strip() for bound_str in group_str.split(",")]
                if start == '?' and end == '?':
                    groups.append(None)
                else:
                    groups.append((int(start), int(end)))
        else:
            continue

        tests.append((name, pattern, text, groups))
    return tests

def main():
    print("//! Generated by make_fowler_tests.py")
    print("")
    print("mod macros;")
    print("")

    tests = read_test_data(sys.argv[1])
    for test in tests:
        print(test_to_str(test))

if __name__ == '__main__':
    main()