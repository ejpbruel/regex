use crate::{
    prog::{InstPtr, Pred, Prog},
    sparse_set::SparseSet,
    text::Cursor,
};

#[derive(Clone, Copy, Debug, Default)]
pub struct Options {
    pub want_first_match: bool,
}

#[derive(Clone, Debug)]
pub struct Cache<P> {
    curr_threads: Threads<P>,
    next_threads: Threads<P>,
    add_thread_stack: Vec<AddThreadFrame<P>>,
}

impl<P> Cache<P> {
    pub fn new(prog: &Prog) -> Self {
        Self {
            curr_threads: Threads::new(prog.insts.len(), prog.slot_count),
            next_threads: Threads::new(prog.insts.len(), prog.slot_count),
            add_thread_stack: Vec::new(),
        }
    }
}

pub fn run<C: Cursor>(
    prog: &Prog,
    cursor: C,
    options: Options,
    slots: &mut [Option<C::Pos>],
    cache: &mut Cache<C::Pos>,
) -> bool {
    Nfa {
        prog,
        cursor,
        want_first_match: options.want_first_match,
        curr_threads: &mut cache.curr_threads,
        next_threads: &mut cache.next_threads,
        add_thread_stack: &mut cache.add_thread_stack,
    }
    .run(slots)
}

#[derive(Debug)]
struct Nfa<'a, C: Cursor> {
    prog: &'a Prog,
    cursor: C,
    want_first_match: bool,
    curr_threads: &'a mut Threads<C::Pos>,
    next_threads: &'a mut Threads<C::Pos>,
    add_thread_stack: &'a mut Vec<AddThreadFrame<C::Pos>>,
}

impl<'a, C: Cursor> Nfa<'a, C> {
    fn run(&mut self, slots: &mut [Option<C::Pos>]) -> bool {
        use {crate::prog::Inst, std::mem};

        let mut matched = false;
        loop {
            if !matched {
                AddThreadView {
                    prog: &self.prog,
                    cursor: &self.cursor,
                    stack: &mut self.add_thread_stack,
                }
                .add_thread(&mut self.next_threads, self.prog.start, slots);
            }
            mem::swap(&mut self.curr_threads, &mut self.next_threads);
            self.next_threads.inst.clear();
            if self.curr_threads.inst.is_empty() {
                break;
            }
            let c = self.cursor.peek_char();
            if let Some(c) = c {
                self.cursor.advance_by(c.len_utf8());
            }
            let mut view = AddThreadView {
                prog: &self.prog,
                cursor: &self.cursor,
                stack: &mut self.add_thread_stack,
            };
            for &inst in self.curr_threads.inst.as_slice() {
                match &self.prog.insts[inst] {
                    Inst::Match => {
                        let len = slots.len().min(self.curr_threads.slots.get(inst).len());
                        (&mut slots[..len])
                            .copy_from_slice(&self.curr_threads.slots.get(inst)[..len]);
                        if self.want_first_match {
                            return true;
                        }
                        matched = true;
                        break;
                    }
                    Inst::ByteRange(_) => panic!(),
                    Inst::Char(inst_ref) => {
                        if c.map_or(false, |c| c == inst_ref.c) {
                            view.add_thread(
                                &mut self.next_threads,
                                inst_ref.out,
                                self.curr_threads.slots.get_mut(inst),
                            );
                        }
                    }
                    Inst::Class(inst_ref) => {
                        if c.map_or(false, |c| inst_ref.class.contains(c)) {
                            view.add_thread(
                                &mut self.next_threads,
                                inst_ref.out,
                                self.curr_threads.slots.get_mut(inst),
                            );
                        }
                    }
                    _ => {}
                }
            }
            if c.is_none() {
                break;
            }
        }
        matched
    }
}

#[derive(Clone, Debug)]
struct Threads<P> {
    inst: SparseSet,
    slots: Slots<P>,
}

impl<P> Threads<P> {
    fn new(inst_count: usize, slot_count: usize) -> Self {
        Self {
            inst: SparseSet::new(inst_count),
            slots: Slots {
                slots: (0..inst_count * slot_count).map(|_| None).collect(),
                slot_count,
            },
        }
    }
}

#[derive(Clone, Debug)]
struct Slots<P> {
    slots: Vec<Option<P>>,
    slot_count: usize,
}

impl<P> Slots<P> {
    fn get(&self, inst: InstPtr) -> &[Option<P>] {
        &self.slots[inst * self.slot_count..][..self.slot_count]
    }

    fn get_mut(&mut self, inst: InstPtr) -> &mut [Option<P>] {
        &mut self.slots[inst * self.slot_count..][..self.slot_count]
    }
}

#[derive(Clone, Debug)]
enum AddThreadFrame<P> {
    AddThread(InstPtr),
    UnsaveSlots(usize, Option<P>),
}

#[derive(Debug)]
struct AddThreadView<'a, C: Cursor> {
    prog: &'a Prog,
    cursor: &'a C,
    stack: &'a mut Vec<AddThreadFrame<C::Pos>>,
}

impl<'a, C: Cursor> AddThreadView<'a, C> {
    fn add_thread(
        &mut self,
        threads: &mut Threads<C::Pos>,
        inst: InstPtr,
        slots: &mut [Option<C::Pos>],
    ) {
        use crate::prog::Inst;

        self.stack.push(AddThreadFrame::AddThread(inst));
        while let Some(frame) = self.stack.pop() {
            match frame {
                AddThreadFrame::AddThread(inst) => {
                    let mut inst = inst;
                    loop {
                        if !threads.inst.insert(inst) {
                            break;
                        }
                        match &self.prog.insts[inst] {
                            Inst::Match | Inst::ByteRange(_) | Inst::Char(_) | Inst::Class(_) => {
                                let len = threads.slots.get(inst).len().min(slots.len());
                                (&mut threads.slots.get_mut(inst)[..len])
                                    .copy_from_slice(&slots[..len]);
                                break;
                            }
                            Inst::Nop(inst_ref) => {
                                inst = inst_ref.out;
                                continue;
                            }
                            Inst::Save(inst_ref) => {
                                if inst_ref.slot_index < slots.len() {
                                    self.stack.push(AddThreadFrame::UnsaveSlots(
                                        inst_ref.slot_index,
                                        slots[inst_ref.slot_index],
                                    ));
                                    slots[inst_ref.slot_index] = Some(self.cursor.pos());
                                }
                                inst = inst_ref.out;
                                continue;
                            }
                            Inst::Assert(inst_ref) => {
                                if match inst_ref.pred {
                                    Pred::TextStart => self.cursor.text_start(),
                                    Pred::TextEnd => self.cursor.text_end(),
                                    Pred::LineStart => self.cursor.line_start(),
                                    Pred::LineEnd => self.cursor.line_end(),
                                    Pred::WordBoundary => self.cursor.word_boundary(),
                                    Pred::NotWordBoundary => !self.cursor.word_boundary(),
                                } {
                                    inst = inst_ref.out;
                                    continue;
                                }
                                break;
                            }
                            Inst::Split(inst_ref) => {
                                self.stack.push(AddThreadFrame::AddThread(inst_ref.out_1));
                                inst = inst_ref.out_0;
                                continue;
                            }
                        }
                    }
                }
                AddThreadFrame::UnsaveSlots(slot_index, old_pos) => slots[slot_index] = old_pos,
            }
        }
    }
}
