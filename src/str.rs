use {
    crate::text::{Cursor, Text},
    std::ops::Range,
};

impl<'a> Text for &'a str {
    type Pos = usize;
    type Curs = Curs<'a>;

    fn curs_front(self, range: Range<Self::Pos>) -> Self::Curs {
        Curs {
            str: self,
            range: range.clone(),
            pos: range.start,
        }
    }

    fn curs_back(self, range: Range<Self::Pos>) -> Self::Curs {
        Curs {
            str: self,
            range: range.clone(),
            pos: range.end,
        }
    }
}

pub struct Curs<'a> {
    str: &'a str,
    range: Range<usize>,
    pos: usize,
}

impl<'a> Cursor for Curs<'a> {
    type Pos = usize;

    fn byte_pos(&self) -> usize {
        self.pos
    }

    fn pos(&self) -> Self::Pos {
        self.pos
    }

    fn peek_byte(&self) -> Option<u8> {
        if self.pos == self.range.end {
            return None;
        }
        Some(self.str.as_bytes()[self.pos])
    }

    fn peek_char(&self) -> Option<char> {
        self.str[self.pos..self.range.end].chars().next()
    }

    fn peek_prev_byte(&self) -> Option<u8> {
        if self.pos == self.range.start {
            return None;
        }
        Some(self.str.as_bytes()[self.pos - 1])
    }

    fn peek_prev_char(&self) -> Option<char> {
        self.str[self.range.start..self.pos].chars().next_back()
    }

    fn advance_by(&mut self, n: usize) {
        assert!(self.range.end - self.pos >= n);
        self.pos += n;
    }

    fn retreat_by(&mut self, n: usize) {
        assert!(self.pos - self.range.start >= n);
        self.pos -= n;
    }
}
