use std::ops::Range;

pub trait Text {
    type Pos: Copy;
    type Curs: Cursor<Pos = Self::Pos>;

    fn curs_front(self, range: Range<Self::Pos>) -> Self::Curs;
    fn curs_back(self, range: Range<Self::Pos>) -> Self::Curs;
}

pub trait Cursor {
    type Pos: Copy;

    fn byte_pos(&self) -> usize;
    fn pos(&self) -> Self::Pos;
    fn peek_byte(&self) -> Option<u8>;
    fn peek_char(&self) -> Option<char>;
    fn peek_prev_byte(&self) -> Option<u8>;
    fn peek_prev_char(&self) -> Option<char>;
    fn advance_by(&mut self, n: usize);
    fn retreat_by(&mut self, n: usize);

    fn text_start(&self) -> bool {
        self.peek_prev_char().is_none()
    }

    fn text_end(&self) -> bool {
        self.peek_char().is_none()
    }

    fn line_start(&self) -> bool {
        self.peek_prev_char().map_or(true, |c| c == '\n')
    }

    fn line_end(&self) -> bool {
        self.peek_char().map_or(true, |c| c == '\n')
    }

    fn word_boundary(&self) -> bool {
        use crate::char::CharExt;

        self.peek_char().map(|c| c.is_word()) != self.peek_prev_char().map(|c| c.is_word())
    }

    fn rev(self) -> RevCursor<Self>
    where
        Self: Sized,
    {
        RevCursor { cursor: self }
    }
}

#[derive(Debug)]
pub struct RevCursor<C> {
    cursor: C,
}

impl<C: Cursor> Cursor for RevCursor<C> {
    type Pos = C::Pos;

    fn byte_pos(&self) -> usize {
        self.cursor.byte_pos()
    }

    fn pos(&self) -> Self::Pos {
        self.cursor.pos()
    }

    fn peek_byte(&self) -> Option<u8> {
        self.cursor.peek_prev_byte()
    }

    fn peek_char(&self) -> Option<char> {
        self.cursor.peek_prev_char()
    }

    fn peek_prev_byte(&self) -> Option<u8> {
        self.cursor.peek_byte()
    }

    fn peek_prev_char(&self) -> Option<char> {
        self.cursor.peek_char()
    }

    fn advance_by(&mut self, n: usize) {
        self.cursor.retreat_by(n)
    }

    fn retreat_by(&mut self, n: usize) {
        self.cursor.advance_by(n)
    }
}
