use {
    crate::{
        compile, dfa, nfa, parse,
        prog::Prog,
        text::{Cursor, Text},
    },
    std::{cell::RefCell, error, fmt, ops::Range, result, sync::Arc},
};

#[derive(Clone, Debug)]
pub struct Regex<P: Copy = usize> {
    shared: Arc<Shared>,
    dfa_cache: RefCell<dfa::Cache>,
    rev_dfa_cache: RefCell<dfa::Cache>,
    nfa_cache: RefCell<nfa::Cache<P>>,
}

impl<P: Copy> Regex<P> {
    pub fn new(pattern: &str) -> Result<Self> {
        println!("REGEX: {:?}", pattern);
        let mut parse_cache = parse::Cache::new();
        let ast = parse::parse(
            pattern,
            parse::Options {
                multiline: true,
                dot_all: true,
                ..parse::Options::default()
            },
            &mut parse_cache,
        )?;
        let mut compile_cache = compile::Cache::new();
        let dfa_prog = compile::compile(
            &ast,
            compile::Options {
                dot_star: true,
                ignore_caps: true,
                byte_based: true,
                ..compile::Options::default()
            },
            &mut compile_cache,
        );
        let rev_dfa_prog = compile::compile(
            &ast,
            compile::Options {
                ignore_caps: true,
                byte_based: true,
                reversed: true,
                ..compile::Options::default()
            },
            &mut compile_cache,
        );
        let nfa_prog = compile::compile(&ast, compile::Options::default(), &mut compile_cache);
        let dfa_cache = dfa::Cache::new(&dfa_prog);
        let rev_dfa_cache = dfa::Cache::new(&rev_dfa_prog);
        let nfa_cache = nfa::Cache::new(&nfa_prog);
        Ok(Self {
            shared: Arc::new(Shared {
                dfa_prog,
                rev_dfa_prog,
                nfa_prog,
            }),
            dfa_cache: RefCell::new(dfa_cache),
            rev_dfa_cache: RefCell::new(rev_dfa_cache),
            nfa_cache: RefCell::new(nfa_cache),
        })
    }

    pub fn run<T: Copy + Text<Pos = P>>(
        &self,
        text: T,
        range: Range<T::Pos>,
        slots: &mut [Option<T::Pos>],
    ) -> bool
    where
        T::Pos: std::fmt::Debug,
    {
        let mut dfa_cache = self.dfa_cache.borrow_mut();
        match dfa::run(
            &self.shared.dfa_prog,
            text.curs_front(range.clone()),
            dfa::Options {
                want_first_match: slots.is_empty(),
                ..dfa::Options::default()
            },
            &mut *dfa_cache,
        ) {
            Ok(Some(end)) => {
                if slots.is_empty() {
                    return true;
                }
                let mut rev_dfa_cache = self.rev_dfa_cache.borrow_mut();
                let start = dfa::run(
                    &self.shared.rev_dfa_prog,
                    text.curs_back(range.start..end).rev(),
                    dfa::Options {
                        want_last_match: true,
                        ..dfa::Options::default()
                    },
                    &mut *rev_dfa_cache,
                )
                .unwrap()
                .unwrap();
                if slots.len() == 2 {
                    slots[0] = Some(start);
                    slots[1] = Some(end);
                } else if slots.len() > 2 {
                    let mut nfa_cache = self.nfa_cache.borrow_mut();
                    nfa::run(
                        &self.shared.nfa_prog,
                        text.curs_front(start..end),
                        nfa::Options::default(),
                        slots,
                        &mut *nfa_cache,
                    );
                }
                true
            }
            Ok(None) => false,
            Err(_) => {
                let mut nfa_cache = self.nfa_cache.borrow_mut();
                nfa::run(
                    &self.shared.nfa_prog,
                    text.curs_front(range),
                    nfa::Options {
                        want_first_match: slots.is_empty(),
                        ..nfa::Options::default()
                    },
                    slots,
                    &mut *nfa_cache,
                )
            }
        }
    }
}

pub type Result<T> = result::Result<T, Error>;

#[derive(Clone, Debug)]
pub enum Error {
    Parse(parse::Error),
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Parse(error) => write!(f, "parse error: {}", error),
        }
    }
}

impl From<parse::Error> for Error {
    fn from(error: parse::Error) -> Self {
        Self::Parse(error)
    }
}

#[derive(Debug)]
struct Shared {
    dfa_prog: Prog,
    rev_dfa_prog: Prog,
    nfa_prog: Prog,
}
